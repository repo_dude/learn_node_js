
const rand = function(n) {
    return Array.from(new Array(10)).map( e=> Math.random())
}

let range = function(start, end){
    if (start<0 || start>=end){
        throw 'Wrong start and end values'
    }
    let range_0 = Array.from(Array(end-start).keys())
    if (start===0) {
        return range_0
    }
    return range_0.map(e => e+start)
}

let a = rand(10)
let b = range(0,10)
let c = range(2,9)
console.log(a)
console.log(b)
console.log(c)