let a = [12, 1, 0, 23, 2, 3, 12, 11]

// (deep) Copy of arrays 
let b = [...a]
let c = Array.from(a)
let d = a.map(_=>_)

console.log(b,c,d )

//Looping
for (let i=0; i<a.length; i++){
    console.log(i, a[i])
}

console.log(' ')
a.forEach((e, i) => {
    console.log(i, e)
})


console.log(' ')
for ( item of a) {
    console.log(item)
}

console.table(a)



// Sorting a list by second name
// Option 1, not efficient, but just for the sake of learning 
let ps = ['Jimi Hendrix', 'Von Halen', 'Eric Lapton']
let sorted = ps.map(e => e.split(' ')).sort((a,b) => a[1]<b[1] ? -1: a[1]>b[1] ? 1:0  ).map(e => `${e[0]} ${e[1]}`)

// Option2 (better), note ps is mutated
ps.sort((a,b) => a.split(' ')[1].localeCompare(b.split(' ')[1]) )



// create array of size N and initialise all elememts to 0
let xarr = new Array(10)
xarr.fill(0)
console.log(xarr)


const peoples = [
    {name:'John Wick', age:34},
    {name:'Paul Dirac', age:84},
    {name:'Stephane Weyl', age:42},
    {name:'James Simpson', age:25},
]

console.table(peoples)