
// var is function scope bound
// let is block scope bound
// const is also block scope bound

// Usen whenever possible const, then let, then var

function sum(arr) {
   let s = 0;
   for (let i = 0; i < arr.length; i++) {
       var x = i;
       let y = i;
       s+= arr[i];
   } 
   console.log(x)
//    console.log(y) throws an error
   return s
}

console.log(sum([1,2,3]))