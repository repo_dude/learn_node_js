var fs = require("fs");
var content = fs.readFileSync('trees.csv', 'utf8');

const data = []
content.split('\n').forEach( e => data.push(e.split(',').map( e=> e.trim())))

const keys = data.shift()
console.log(data)

let obj_data = []
for (const elem of data) {
    // create object by zipping two lists
    if (elem.length>1) {
        obj_data.push(Object.fromEntries( keys.map((e,i) => [e,parseInt(elem[i])]) ))
    }
}

console.table(obj_data)
