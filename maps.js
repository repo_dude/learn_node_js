const map1 = new Map();

map1.set('a', 1);
map1.set('b', 2);
map1.set('c', 3);
console.log(map1)
    

console.log(map1.get('a'));
// expected output: 1

map1.set('a', 97);

console.log(map1.get('a'));
// expected output: 97

console.log(map1.size);
// expected output: 3

map1.delete('b');

console.log(map1.size);
// expected output: 2

const map2 = new Map()
map2.set(1,2)
console.log(map2)




// ------------------
const myMap= new Map()
myMap.set(0, 'zero')
myMap.set(1, 'one')

for (const [key, value] of myMap) {
  console.log(key + ' = ' + value)
}
// 0 = zero
// 1 = one

for (const key of myMap.keys()) {
  console.log(key)
}
// 0
// 1

for (const value of myMap.values()) {
  console.log(value)
}
// zero
// one

for (const [key, value] of myMap.entries()) {
  console.log(key + ' = ' + value)
}
// 0 = zero
// 1 = one



const kvArray = [['key1', 'value1'], ['key2', 'value2']]

// Use the regular Map constructor to transform a 2D key-value Array into a map
const myMap2 = new Map(kvArray)

myMap2.get('key1') // returns "value1"

// Use Array.from() to transform a map into a 2D key-value Array
console.log(Array.from(myMap2)) // Will show you exactly the same Array as kvArray

// A succinct way to do the same, using the spread syntax
console.log([...myMap2])

// Or use the keys() or values() iterators, and convert them to an array
console.log(Array.from(myMap2.keys())) // ["key1", "key2"]



const original = new Map([
    [1, 'one']
  ])
  
  const clone = new Map(original)
  
  console.log(clone.get(1))       // one
  console.log(original === clone) // false (useful for shallow comparison)

  const first = new Map([
    [1, 'one'],
    [2, 'two'],
    [3, 'three'],
  ])
  
  const second = new Map([
    [1, 'uno'],
    [2, 'dos']
  ])
  
  // Merge maps with an array. The last repeated key wins.
  const merged = new Map([...first, ...second, [1, 'eins']])
  
  console.log(merged.get(1)) // eins
  console.log(merged.get(2)) // dos
  console.log(merged.get(3)) // three
  