var stats =  require('./modules/stats.js');

// let text =`
// y,AMI,GEN,AMT,PR,DIAP,QRS
// 3389,3149,1,7500,220,0,140
// 1101,653,1,1975,200,0,100
// 1131,810,0,3600,205,60,111
// 596,448,1,675,160,60,120
// 896,844,1,750,185,70,83
// 1767,1450,1,2500,180,60,80
// 807,493,1,350,154,80,98
// 1111,941,0,1500,200,70,93
// 645,547,1,375,137,60,105
// 628,392,1,1050,167,60,74
// 1360,1283,1,3000,180,60,80
// 652,458,1,450,160,64,60
// 860,722,1,1750,135,90,79
// 500,384,0,2000,160,60,80
// 781,501,0,4500,180,0,100
// 1070,405,0,1500,170,90,120
// 1754,1520,1,3000,180,0,129

// `

let text=`
x,    y 
1 ,   4 
1.5 , 7 
6 ,   12
2 ,   8 
3,    7 
`


console.log(text.trim())
function extract_X_y(text, target_name){
    let dt = text.split('\n').filter(e => e.replace(' ','')!=''); 
    console.log(dt)
    let header = dt.shift()
    header = header.split(',').map(e =>  e.trim() )
    console.log(header)

    let cols = {}
    // {var1:0, var2:1, ...}
    header.forEach((e,i)=> cols[e]=i)
    console.log(cols)
    let X = dt.map( e=>e.split(',').map(r => Number(r)))
    console.table(X)

    let target = target_name
    let target_index = cols[target];
    let y = X.map( e => e.splice(target_index,1)).flat()
    return {X:X, y:y}
}


let {X, y} = extract_X_y(text,'y')

let Reg =  new stats.Regressor(X,y)
Reg.train()
console.log('Regression Coefs:')
console.log(Reg.coef)
console.log('Predicted values yhat:')
let yhat = Reg.predict(X)
console.table(yhat)

let r2 = Reg.r_squared()
console.log(`R-Squared = ${r2}`)





