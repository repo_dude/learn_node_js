//----------- Call
let obj = {name:'Karl', age:35};
function effective_working_time(rate, offset){
    return this.age * rate + offset;
}
// calling the function on the object obj
let eff_work = effective_working_time.call(obj, 0.8, 1)
console.log(eff_work)

// ---- apply , the same, parameters in an array
eff_work = effective_working_time.apply(obj, [0.8, 1])
console.log(eff_work)

// ---- bind
let bounded = effective_working_time.bind(obj)
console.dir(bounded)

eff_work = bounded(0.8, 1)
console.log(eff_work)


