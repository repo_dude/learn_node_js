let a = [3, 10, 12, 6,71, 11, 54, 71, 100]
console.log(a)

// Python: any([r > 20 for r in a]) 
// or b = any([_ > 20 for _ in a])
let b = a.some( e => e >20)
console.log(b)

// Python: c = all([r > 20 for r in a])
let c = a.every( e => e >20)
console.log(c)


// Python: d = [ r**2 for r in a]
let d = a.map( e => e**2)
console.log(d)

// Python: a.remove(71)
a.splice(a.indexOf(71),1)
console.log(a)

// Python:  12 in a 
console.log(a.includes(12))

// -Destructuring an array
// Python a1, a2, *a3 = a 
let [a1, a2, ...a3] = a
console.log(a1, a2, a3)

// -Destructuring an object
// No exact python equivalent
let paul = {name:'Paul', age:35}
let {name, age} = paul
console.log( name, age)


// creating list with uniq elements 
// Python:  list(set(a))
let u  = [...new Set(a)]
console.log(u)


// Iterator 
// Create a map/dict  var_1:3, var_2:10, ...
// Python:
// z = [3, 10, 5, 7]
// z2 = {f'var_{i+1}':r for i,r in enumerate(z)}

z = [3, 10, 5, 7]
let z2 = new Map()
z.forEach( (e, i) => z2.set(`var_${i+1}`,e) )