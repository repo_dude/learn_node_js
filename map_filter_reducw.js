

let nums = [3, 10, 21, 6, 7, 100, 32];

// print elements and indexes
// three parameters can be accessed, the array's elements, indexes, and the array itself
// (item, index, elems)  
nums.map( (item, index, elems) => 
           console.log(`index= ${index}  item= ${item}  Array length = ${elems.length}`)
          )
// Can also use forEach
// nums.forEach((item, index, elems) => 
//             console.log(`index= ${index}  item= ${item}  Array length = ${elems.length}`))          
// index= 0  item= 3  Array length = 7
// index= 1  item= 10  Array length = 7
// index= 2  item= 21  Array length = 7
// index= 3  item= 6  Array length = 7
// index= 4  item= 7  Array length = 7
// index= 5  item= 100  Array length = 7
// index= 6  item= 32  Array length = 7

let nums_square = nums.map((item)=> item ** 2)
// [
//     9, 100,   441,
//    36,  49, 10000,
//  1024
// ]

console.log(nums_square)

let nums_gt_7 = nums.filter((item)=> item >7 )
console.log(nums_gt_7)

// 0 indicates the initial value
let sum = nums.reduce((prev, curr)=> prev+curr, 0)
console.log(sum)
// 179



// sorting by specific property 
const employees = [    
    {name:"Ram", email:"ram@gmail.com", age:23},    
    {name:"Shyam", email:"shyam23@gmail.com", age:28},  
    {name:"John", email:"john@gmail.com", age:33},    
    {name:"Bob", email:"bob32@gmail.com", age:41}   
]

// console.log(employees)

// sorting by name 
employees.sort( (a, b) => a.name < b.name? -1: a.name>b.name ? 1:0 )
console.log(employees)