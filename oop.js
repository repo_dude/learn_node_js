class Person {
    constructor(name){
        this.name = name;
    }

    sayHello(){
        console.log(`Hello, this is ${this.name}`);
    }
}

p = new Person("Hamlet")
p.sayHello();

console.log(p.name) // is accessible as it is public

// private properties and methods 
console.log("")

class Person2 {

    #age; // age is private, needs to be 'declared here'

    constructor(name, age){
        this.name = name;
        this.#age = age; 
        if (!this.#checkAge()){
            throw new Error("Age out of bounds.");
        }
    }
    // private method
    #checkAge(){
        if (18<= this.#age && this.#age<= 65) return true;
        return false;
    }

    sayHello(){
        console.log(`Hello, this is ${this.name}`);
    }

    static square(x) {
        return x**2 
    }
}

// p2 = new Person2("Adam", 84); this will throw an error as exepcted
//p2.sayHello()

console.log("")

class Employee extends Person {

    constructor(name, profession, salary){
        super(name);
        this.profession = profession;
        this.salary = salary;
    }

    calculateBonus(rate){
        return (1+rate)*this.salary;
    }

}

p3 = new Employee("Fred", 35, 30000);
p3.sayHello()
console.log(`Bonus for this year is ${p3.calculateBonus(0.10)}`)

// static mehtod
console.log(Person2.square(20))