// let a = [3,1,4,5,5,1,2,3,8,9,8,0]

let text = `
Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet
`

let a = text.split(' ').map(e => e.replace(',',' ').replace('.',' ').trim().toLowerCase())

let uniq = [...new Set(a)]

let cnt = uniq.map( e => a.filter( x=> x===e ).length)

let counts = new Map(uniq.map((e,i)=>[e,cnt[i]]) )

// console.log(counts)

let desc_sorted = new Map([...counts.entries()].sort((a,b) => b[1]-a[1]))

console.log(desc_sorted)