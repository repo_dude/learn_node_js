
function Car(make, cc){
  
   let  model = 'Any' // this is a private variable  
   this.make = make
   this.engineSize = cc

   this.printinfo = function() {
       console.log(this.make);
   }


}

let car1 = new Car('Volvo')
car1.printinfo()

// Car function is object
console.log(Car.length) // number of parameters