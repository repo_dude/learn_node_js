var mat = require('./matrix.js');


const min = arr => Math.min(...arr);

const arg_min = arr => arr.indexOf(Math.min(...arr))

const max = arr => Math.max(...arr);

const arg_max = arr => arr.indexOf(Math.max(...arr))

const sum = arr => arr.reduce((cumm, curr)=> cumm+curr, 0);

const mean = arr => sum(arr)/arr.length;

function median(arr){
    x = [...arr];
    x.sort((a,b)=> a-b)
    let n = x.length;
    let ind = Math.trunc(n/2);
    if (n%2===0) {return (x[ind-1]+x[ind])/2};
    return x[ind+1]
}

const variance = function(arr){
    let m = mean(arr)
    let vr = sum(arr.map(x => (x-m)**2))/arr.length
    return vr
}

const stdev = function(arr, sample = true) {
    const m = mean(arr);
    const n = arr.length;
    let ss = sum(arr.map(x => (x-m)**2))
    ss = sample? ss/(n-1): ss/n 
    return Math.sqrt(ss)
}

const describe = function(arr){
    return {n:arr.length, 
            min: min(arr),
            max: max(arr),
            mean: mean(arr),
            std: stdev(arr)
        }
}

const rediduals = function(y, yhat){
    if(yhat.length!=y.length) throw 'dimension error'
    return y.map((e,i) => e - yhat[i] )
}

const SSE = function(y, yhat){
    let res = rediduals(y,yhat)
    return res.reduce((cumm, curr)=> cumm+curr**2,0 )
}

const SST = function(y){
    let m = mean(y)
    let ym = y.map(e => m ) // [m, m,...]
    return SSE(y, ym)
}


function Regressor(X, y){
    this.X = X
    this.y = y
    
    this.train = function(){
        let aug_mat = this.X.map(e => [...e, 1]) 
        let X_1 = mat.Matrix_from(aug_mat)

        let lhs = mat.matmul(X_1.transpose(), X_1) 
        let rhs = X_1.transpose().times(this.y)
        this.coef = mat.lsolve(lhs, rhs)
    }
    
    this.predict= function(X){
        let X_1 = mat.Matrix_from(X.map(e => [...e, 1]))
        return X_1.times(this.coef)
    }

    this.r_squared = function(){
        let yhat = this.predict(this.X)
        return    1-SSE(this.y, yhat)/SST(this.y)
    }

    return this
}



module.exports= {min, arg_min, max, arg_max, sum, mean, median, 
    stdev, describe, Regressor, rediduals, SSE, SST, variance};