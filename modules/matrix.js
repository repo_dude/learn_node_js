function Matrix(n, m=n){ 
    this.val = new Array(n)
    for(let i=0; i<n; i++){
        this.val[i] = new Array(m).fill(0)
    }
    this.n_rows = n
    this.n_cols = m 
    return this
}

function Matrix_from(arr){
    /**
     * Create Matrix from Array of arrys
     * @arr: array of arrays [[*,*...],...]
     */
    let n = arr.length
    let m  = arr[0].length
    let a = new Matrix(n,m)
    for(let i in a.val){
        a.val[i]=arr[i]
    }
    return a
}

// filling with random number
Matrix.prototype.rand_fill = function(){
    for(const i in this.val){
        for (const j in this.val[i]) {
            this.val[i][j] = Math.random()
        }
    }
}

// filling with a fixed value x
Matrix.prototype.fill = function(x){
    for (const i in this.val) {
        this.val[i].fill(x)
    }
}

Matrix.prototype.transpose = function() {
    // A -> At
    const t = new Matrix(this.n_cols, this.n_rows)
    for(let i=0; i<this.n_rows; i++){
        for(let j=0; j<this.n_cols; j++) {
            t.val[j][i] = this.val[i][j]
        }
    }
    return t 
}

// dot product of two vectors
function dot(a, b){
    if (a.length != b.length) {
        throw 'dot(a,b) a and b must have the same length'
    }
    return a.map((_,i)=>a[i]*b[i]).reduce((x, y)=>x+y,0)
}

Matrix.prototype.times = function(b) {
    // A*b  where A is 'this' b is 1d array
    if(this.n_cols!=b.length){
        throw 'Wrong dimensions'
    }
    return this.val.map( (row,_) => dot(row,b))
}

// Generates identity matrix of size n
function eye(n){
    let unit = new Matrix(n,n)
    unit.val.forEach((r,i)=> r[i]=1.0)
    return unit
}

function matmul(A, B){
    if (A.n_cols!=B.n_rows) {
        throw 'Dimension of A.n_cols != B.n_rows'
    }
    let k_max = A.n_cols
    C = new Matrix(A.n_rows, B.n_cols)
    for( let i =0; i<A.n_rows; i++){
        for(let j=0; j<B.n_cols; j++){
            for(let k=0; k<k_max; k++) {
                C.val[i][j] += A.val[i][k]*B.val[k][j]
            }
        }
    }
    return C 
}

function lsolve(A,  b) {
    /**
     *  Solving a System of Linear equations
     *  using Gaussian elemination with pivoting
     */
    const EPSILON = 1e-10
    const n = b.length
    for (let p = 0; p < n; p++) {
        // find pivot row and swap
        let max = p;
        for (let i = p + 1; i < n; i++) {
            if (Math.abs(A.val[i][p]) > Math.abs(A.val[max][p])) {
                max = i;
            }
        }
        let temp = A.val[p]; 
        A.val[p] = A.val[max]; 
        A.val[max] = temp;
        let t=b[p]; b[p]=b[max]; b[max]=t;
        // singular or nearly singular
        if (Math.abs(A.val[p][p]) <= EPSILON) {
            throw "Matrix is singular or nearly singular"
        }
        // pivot within A and b
        for (let i = p + 1; i < n; i++) {
            let alpha = A.val[i][p] / A.val[p][p];
            b[i] -= alpha * b[p];
            for (let j = p; j < n; j++) {
                A.val[i][j] -= alpha * A.val[p][j];
            }
        }
    }
    // back substitution
    let x = new Array(n).fill(0);
    for (let i = n - 1; i >= 0; i--) {
        let sum = 0.0;
        for (let j = i + 1; j < n; j++) {
            sum += A.val[i][j] * x[j];
        }
        x[i] = (b[i] - sum) / A.val[i][i];
    }
    return x;
}




/*
const a = new Matrix(4,3)

a.rand_fill()

console.table(a.val)

console.log(a.val[1][1])

a.fill(1.5)

console.table(a.val)

let e = eye(3)

console.table(e.val)

let A = Matrix_from([[2, 5, 8, 7], [5, 2, 2, 8], [7, 5, 6, 6], [5, 4, 4, 8]])
let b = [1,1,1,1]
console.table(A.val)
console.table(b)

let x = lsolve(A, b)
console.table(x)

b = [1,1,1,1]
A = Matrix_from([[2, 5, 8, 7], [5, 2, 2, 8], [7, 5, 6, 6], [5, 4, 4, 8]])
let t = matmul(A, eye(4))
console.table(t.val)

let At = t.transpose()
console.table(At.val)

let c = A.times(b)
console.log(c)

// Regression exampe

x = [1, 1.5, 6, 2, 3]
let y = [4, 7, 12, 8, 7]


let Reg = new Regressor(x,y)
Reg.train()
console.log('Regression Coefs:')
console.log(Reg.coef)
let P = Reg.predict(x)

console.table(P)
*/

module.exports = {Matrix, Matrix_from, lsolve, dot, matmul, eye}