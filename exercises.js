// Palindrome
const isPalendrome = s => s.split('').reverse().join('')===s ;
console.log(isPalendrome('racecar'))

// Find min max of an array 

const getMinMax = function(arr) {
    min =arr.reduce((cumm,curr) => curr>cumm? cumm:curr, Number.MAX_VALUE)
    max = arr.reduce((cumm,curr) => curr<cumm? cumm:curr, Number.MIN_VALUE)
    return {min, max}
}

// simpler and efficient
const getMinMax2 = function(arr) {
    return {
        min: Math.min(...a),
        max: Math.max(...a)
    }
}

let a = [9, 0, 12, 34, -23, 4, 10]

console.log(getMinMax(a))
console.log(getMinMax2(a))


// Verify if a string has balanced parenthesis
// const isBalanced = s => s.match(/\(/g).length === s.match(/\)/g).length
function isBalanced(s){
    let left = 0
    let right = 0
    let balanced = true
    while (balanced) {
        left = s.indexOf('(')
        right = s.lastIndexOf(')')
        balanced = left<right
        
    }

}

console.log(isBalanced('This is( not (trivial) )'))
console.log(isBalanced('a car(( not really)'))
