

var stats =  require('./modules/stats.js');

let a = [4, 0, -1, 10, 3,4]

console.log(stats.mean(a))

console.log(stats.median(a))

console.log(stats.stdev(a))

console.table([stats.describe(a)])

// Regression 
// reason x values are expressed as [x], this for a general case for more than one dimension
// let x = [[1], [1.5], [6], [2], [3]]
// let y = [4, 7, 12, 8, 7]

let x = [[5], [15], [25], [35], [45], [55]]
let y = [5, 20, 14, 32, 22, 38]


let Reg =  new stats.Regressor(x,y)
Reg.train()
console.log('Regression Coefs:')
console.log(Reg.coef)
let P = Reg.predict(x)
console.table(P)

let r2 = Reg.r_squared()
console.log(`R-Squared = ${r2}`)
