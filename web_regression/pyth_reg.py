import numpy as np
from sklearn.linear_model import LinearRegression
x = np.array([5, 15, 25, 35, 45, 55]).reshape((-1, 1))
y = np.array([5, 20, 14, 32, 22, 38])

model = LinearRegression()
model.fit(x,y)

r_sq = model.score(x, y)
print("Coefficients: \n", model.coef_)
print('coefficient of determination:', r_sq)
# coefficient of determination: 0.715875613747954
ajs_r2 = 1 - ( 1-model.score(x, y) ) * ( len(y) - 1 ) / ( len(y) - x.shape[1] - 1 )
print(ajs_r2)