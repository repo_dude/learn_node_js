// ** Gini Index inequality 
// ? G = (n+1)/n - 2*sum((n+1-i)*xi)/(n*sum(xi)), i= 1,..,n
// ! array must be sorted
const x = [1000,2000,3000,4000,5000,6000,7000]

let n = x.length

// not the best way, but just for learning
let nomin = 2*x.map((xi, i)=>(n+1-(i+1))*xi).reduce((a,b)=> a+b, 0)
let denom = n*x.reduce((a,b)=>a+b, 0)
let G = (n+1)/n - nomin/denom
console.log(G)

const sum = arr => arr.reduce((a,b)=> a+b, 0)

// neater way 
nomin = 2*sum(x.map((xi, i)=>(n+1-(i+1))*xi))
denom = n*sum(x)
G = (n+1)/n - nomin/denom
console.log(G)
