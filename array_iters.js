// equivalant of Python range function

//range(10)
a = Array.from(Array(10).keys())
console.log(a)

//range(10)
b = [...Array(10).keys()]
console.log(b)

// range(1,11)           // populate array values with index +1
c = Array.from({length: 10}, (_, i) => i + 1)
console.log(c)

