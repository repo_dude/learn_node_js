
const f = x => x**2


function integ( func, a, b, n) {
    const h = (b-a)/n 
    let s = 0.0
    for (let i = 1; i < n; i++) {
        s+= f(a+i*h)*h  
    }
    s += (a+b)*h/2 
    return s 
}

let res = integ(f, 0, 1, 100)

console.log(res)