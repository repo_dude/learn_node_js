var library = [ 
    { author: 'Bill Gates', title: 'The Road Ahead', libraryID: 1254},
    { author: 'Steve Jobs', title: 'Walter Isaacson', libraryID: 4264},
    { author: 'Suzanne Collins', title: 'Mockingjay: The Final Book of The Hunger Games', libraryID: 3245}
    ];

/*
// option 1
const compTitle = (a, b) => {
    if (a.title < b.title) return -1
    if (a.title > b.title) return 1
    return 0
}
library.sort(compTitle)
*/

/*
// option 2 
function compTitle(a, b)  {
    if (a.title < b.title) return -1
    if (a.title > b.title) return 1
    return 0
}
library.sort(compTitle)
*/

// console.log()

// option 3
library.sort((a, b)=> a.title < b.title ? -1 : a.title >b.title ? 1 : 0 )

console.log(library)

