// Destructuring Nested objects
const Person = {
    name: "Rezaul karim",
    age: 23,
    sex: "male",
    maritalstatus: "single",
    address: {
        country: "BD",
        state: "Dhaka",
        city: "N.Ganj",
        pincode: "123456",
    },
};
const { address: { state, pincode }, name } = Person;
console.log(name, state, pincode)


// merge two object
let a = { name: 'Hi', age: 30 }
let b = {address:'Street', number:19}

let c = {...a, ...b}
console.log(c)
